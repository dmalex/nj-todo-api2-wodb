
const app = require('../app')
const Todo = require('../models/todo')

// Retrieve all Todos
exports.getTodos = ( (req, res) => {
    Todo.getTodos((err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Some error occured while retrieving todos."
            })
        } else {
            res.setHeader('Content-Type', 'application/json')
            res.send(data)
        }
    })
})


// Retrieve a single Todo by id
exports.getTodoByID = ( (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content cannot be empty."
    })
  }
  Todo.getTodoByID(req.body.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found todo with id ${req.body.id}.`
                })
            } else {
                res.status(500).send({
                    message: "Error retrieving todo with id " + req.body.id + "."
                })
            }
        } else res.send(data)
  })
})


// Create a new Todo
exports.postTodoByName = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content cannot be empty."
    })
  }
  // Create a Todo
  const todo = new Todo({
    name: req.body.name,
  })
  // Save Todo in the database
  Todo.postTodoByName(todo, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Todo."
      })
    else res.send(data)
  })
}


// Update a Todo by id
exports.putTodoNameByID = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content cannot be empty."
    })
  }
  Todo.putTodoNameByID(
    req.body.id,
    new Todo(req.body), (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Todo with id ${req.body.id}`
          })
        } else {
          res.status(500).send({
            message: "Error updating Todo with id " + req.body.id
          })
        }
      } else res.send(data)
    }
  )
}


// Delete a Todo by id
exports.deleteTodoByID = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content cannot be empty"
    })
  }
  Todo.deleteTodoByID(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Todo with id ${req.params.id}.`
        })
      } else {
        res.status(500).send({
          message: "Could not delete Todo with id " + req.params.id + "."
        })
      }
    } else res.send({ message: `Todo with id ${req.params.id} was deleted successfully.` })
  })
}


// Delete all Todos
exports.deleteTodos = (req, res) => {
    Todo.deleteTodos((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all Todos."
        })
      else res.send({ message: `All Todos were deleted successfully.` })
    })
}


// Update partially a Todo by id
exports.patchTodoNameByID = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content cannot be empty."
    })
  }
  Todo.patchTodoNameByID(
    req.params.id,
    new Todo(req.body), (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Todo with id ${req.params.id}.`
          })
        } else {
          res.status(500).send({
            message: "Error updating partially Todo with id " + req.params.id + "."
          })
        }
      } else res.send(data)
    }
  )
}
