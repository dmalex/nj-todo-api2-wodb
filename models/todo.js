/// todo.js
/// todo model
/// Dmitry Alexandrov

const app = require('../app')

const Todo = function(todo) {
    // this.id = todo.id
    this.name = todo.name
}


function getRandomInt() {
  return Math.floor(Math.random() * 100000);
}


Todo.postTodoByName = (newTodo, result) => {
  const rid = getRandomInt()
  app.todos = [{id: rid, name: newTodo.name}, ...app.todos]
  console.log("Created todo: ", { id: rid, ...newTodo })
  result(null, { id: rid, ...newTodo })
}


Todo.putTodoNameByID = (id, todo, result) => {
  let found = false
  app.todos = app.todos.map(t => {
    if (t.id === id) {
      t.name = todo.name
      found = true
    }
    return t
  })

  // console.log(typeof(id)) // number

  if (!found) {
    console.log("Error: not found Todo with id =", id)
    result({ kind: "not_found" }, null)
    return
  }
  console.log("Updated todo: ", { id: id, ...todo })
  result(null, { id: id, ...todo })
}


Todo.getTodos = (result) => {
  const allTodos = app.todos
    
  if (allTodos.length) {
    console.log("Found todos:", allTodos)
  }
  result(null, allTodos)
}


Todo.getTodoByID = (id, result) => {
  const todo = app.todos.find(t => t.id === id)

  if (todo) {
    console.log("Found todo:", todo)
    result(null, todo)
    return
  }
  result( { kind: "not_found" } , null)
}


Todo.deleteTodoByID = (id, result) => {
  const iden = Number(id)
  const lenBeforeDelete = app.todos.length
  app.todos = app.todos.filter(todo => todo.id !== iden)
  const lenAfterDelete = app.todos.length

  if (lenBeforeDelete === lenAfterDelete) { // Not found todo with id
    result({ kind: "not_found" }, null)
    return
  }
  console.log("Deleted todo with id =", id)
  result(null, app.todos)
}


Todo.deleteTodos = result => {
  const lenBeforeDelete = app.todos.length
  console.log(`Deleted ${lenBeforeDelete} todos`)
  app.todos = []
  result(null, [])
}


Todo.patchTodoNameByID = (id, todo, result) => {
  const iden = Number(id)
  let found = false
  app.todos = app.todos.map(t => {
    if (t.id === iden) {
      // console.log(typeof(t.name)) // string
      t.name = todo.name
      found = true
    }
    return t
  })

  // console.log(typeof(id)) // string
  // console.log(typeof(iden)) // number
  // console.log(typeof(todo.name)) // string
  // console.log(typeof(app.todos[0].id)) // number

  if (!found) {
    console.log("Error: not found Todo with id =", id)
    result({ kind: "not_found" }, null)
    return
  }
  console.log("Updated partially todo: ", { id: id, ...todo })
  result(null, { id: id, ...todo })
}


module.exports = Todo
